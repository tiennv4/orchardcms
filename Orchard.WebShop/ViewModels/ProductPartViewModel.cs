﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Orchard.WebShop.Models;
using Orchard.WebShop.Settings;
using OrchardCore.ContentManagement;


namespace Orchard.WebShop.ViewModels
{
    public class ProductPartViewModel
    {
        public string Name { get; set; }
        public string MySetting { get; set; }
        public bool Show { get; set; }
        public decimal UnitPrice { get; set; }
        public string Sku { get; set; }

        [BindNever]
        public ContentItem ContentItem { get; set; }

        [BindNever]
        public ProductPart ProductPart { get; set; }

        [BindNever]
        public ProductPartSettings Settings { get; set; }
    }
}
