﻿using OrchardCore.ContentManagement;


namespace Orchard.WebShop.Models
{
    public class ProductPart : ContentPart
    {
        public string Name { get; set; }
        public decimal UnitPrice { get; set; }
        public string Sku { get; set; }
        public bool Show { get; set; }
    }
}
