﻿using Orchard.WebShop.Models;
using Orchard.WebShop.Settings;
using Orchard.WebShop.ViewModels;
using OrchardCore.ContentManagement.Display.ContentDisplay;
using OrchardCore.ContentManagement.Metadata;
using OrchardCore.DisplayManagement.ModelBinding;
using OrchardCore.DisplayManagement.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchard.WebShop.Drivers
{
    public class ProductPartDisplayDriver : ContentPartDisplayDriver<ProductPart>
    {
        private readonly IContentDefinitionManager _contentDefinitionManager;

        public ProductPartDisplayDriver(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public override IDisplayResult Display(ProductPart productPart)
        {
            return Combine(
                Initialize<ProductPartViewModel>("ProductPart", m => BuildViewModel(m, productPart))
                    .Location("Detail", "Content:10"),
                Initialize<ProductPartViewModel>("ProductPart_Summary", m => BuildViewModel(m, productPart))
                    .Location("Summary", "Meta:10")
            );
        }

        public override IDisplayResult Edit(ProductPart productPart)
        {
            return Initialize<ProductPartViewModel>("ProductPart_Edit", m => BuildViewModel(m, productPart));
        }

        public override async Task<IDisplayResult> UpdateAsync(ProductPart model, IUpdateModel updater)
        {
            var settings = GetProductPartSettings(model);

            await updater.TryUpdateModelAsync(model, Prefix, t => t.Show, t=>t.Name, t=>t.Sku, t=>t.UnitPrice);

            return Edit(model);
        }

        public ProductPartSettings GetProductPartSettings(ProductPart part)
        {
            var contentTypeDefinition = _contentDefinitionManager.GetTypeDefinition(part.ContentItem.ContentType);
            var contentTypePartDefinition = contentTypeDefinition.Parts.FirstOrDefault(p => p.PartDefinition.Name == nameof(ProductPart));
            var settings = contentTypePartDefinition.GetSettings<ProductPartSettings>();

            return settings;
        }

        private Task BuildViewModel(ProductPartViewModel model, ProductPart part)
        {
            var settings = GetProductPartSettings(part);

            model.ContentItem = part.ContentItem;
            model.MySetting = settings.MySetting;
            model.Show = part.Show;
            model.UnitPrice = part.UnitPrice;
            model.Sku = part.Sku;
            model.ProductPart = part;
            model.Settings = settings;
            model.Name = part.Name;

            return Task.CompletedTask;
        }

    }
}
