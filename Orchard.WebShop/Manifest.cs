﻿using OrchardCore.Modules.Manifest;

[assembly: Module(
    Name = "OrchardCore Templates Cms Module of WebShop",
    Author = "Tiennv",
    Website = "https://orchardcore.net",
    Version = "0.0.1",
    Description = "The simple Orchard template cms module of WebShop",
#if (AddPart)
    Dependencies = new[] { "OrchardCore.Contents" },
#endif
    Category = "Content Management"
)]