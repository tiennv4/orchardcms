﻿using Orchard.WebShop.Models;
using OrchardCore.ContentManagement.Handlers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orchard.WebShop.Handlers
{
    public class ProductPartHandlers : ContentPartHandler<ProductPart>
    {
        public override Task InitializingAsync(InitializingContentContext context, ProductPart instance)
        {
            instance.Show = true;
            instance.UnitPrice = 1000;
            instance.Sku = "SKU_1000";
            return Task.CompletedTask;
        }
    }
}
