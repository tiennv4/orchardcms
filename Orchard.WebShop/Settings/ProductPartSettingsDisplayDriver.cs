﻿using Orchard.WebShop.Models;
using OrchardCore.ContentManagement.Metadata.Models;
using OrchardCore.ContentTypes.Editors;
using OrchardCore.DisplayManagement.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orchard.WebShop.Settings
{
    public class ProductPartSettingsDisplayDriver : ContentPartDefinitionDisplayDriver
    {
        public override IDisplayResult Edit(ContentPartDefinition contentPartDefinition)
        {
            if (!String.Equals(nameof(ProductPart), contentPartDefinition.Name))
            {
                return null;
            }

            return Initialize<ProductPartSettingsViewModel>("ProductPartSettings_Edit", model =>
            {
                var settings = contentPartDefinition.GetSettings<ProductPartSettings>();

                model.MySetting = settings.MySetting;
                model.ProductPartSettings = settings;
            }).Location("Content");
        }

        public override async Task<IDisplayResult> UpdateAsync(ContentPartDefinition contentPartDefinition, UpdatePartEditorContext context)
        {
            if (!String.Equals(nameof(ProductPart), contentPartDefinition.Name))
            {
                return null;
            }

            var model = new ProductPartSettingsViewModel();

            if (await context.Updater.TryUpdateModelAsync(model, Prefix, m => m.MySetting))
            {
                context.Builder.WithSettings(new ProductPartSettings { MySetting = model.MySetting });
            }

            return Edit(contentPartDefinition);
        }
    }
}

