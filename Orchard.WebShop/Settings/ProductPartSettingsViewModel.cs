﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Orchard.WebShop.Settings
{
    public class ProductPartSettingsViewModel
    {
        public string MySetting { get; set; }

        [BindNever]
        public ProductPartSettings ProductPartSettings { get; set; }
    }
}
