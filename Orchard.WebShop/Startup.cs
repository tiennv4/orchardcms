﻿using System;
using Fluid;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Orchard.WebShop.Drivers;
using Orchard.WebShop.Handlers;
using Orchard.WebShop.Models;
using Orchard.WebShop.Settings;
using Orchard.WebShop.ViewModels;
using OrchardCore.ContentManagement;
using OrchardCore.ContentManagement.Display.ContentDisplay;
using OrchardCore.ContentTypes.Editors;
using OrchardCore.Data.Migration;
using OrchardCore.Modules;

namespace Orchard.WebShop
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<ProductPartViewModel>();
            TemplateContext.GlobalMemberAccessStrategy.Register<ProductPartSettingsViewModel>();
        }
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddContentPart<ProductPart>()
               .UseDisplayDriver<ProductPartDisplayDriver>()
               .AddHandler<ProductPartHandlers>();

            services.AddScoped<IContentPartDefinitionDisplayDriver, ProductPartSettingsDisplayDriver>();
            services.AddScoped<IDataMigration, Migrations>();
        }
    }
}
