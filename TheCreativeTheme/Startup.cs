﻿using Microsoft.Extensions.DependencyInjection;
using OrchardCore.Modules;
using OrchardCore.ResourceManagement;


namespace TheCreativeTheme
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            //base.ConfigureServices(services);
            services.AddScoped<IResourceManifestProvider, ResourceManifest>();
        }
    }
}
