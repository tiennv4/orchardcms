using OrchardCore.DisplayManagement.Manifest;

[assembly: Theme(
    Name = "The Creative Theme",
    Author = "Tiennv",
    Website = "https://OrchardCMS.net",
    Version = "2.0.1",
    Description = "An OrchardCMS.net theme adapted for Creative websites."
)]
